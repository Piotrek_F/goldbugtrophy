package pl.codehussar.goldbugtrophy.model;

import java.io.Serializable;
import java.util.List;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
public class User implements Serializable {

    String id;
    List<InformationsPerUrl> informationsPerUrls;
    List<InformationsPerUrl> informationsPerUrlsFalse;
}
