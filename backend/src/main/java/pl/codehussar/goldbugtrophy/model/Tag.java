package pl.codehussar.goldbugtrophy.model;

import java.io.Serializable;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
public class Tag implements Serializable {

  Integer count;
  String name;

}
