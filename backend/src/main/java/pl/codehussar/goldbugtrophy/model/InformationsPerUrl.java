package pl.codehussar.goldbugtrophy.model;

import java.io.Serializable;
import java.util.List;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
public class InformationsPerUrl implements Serializable {

  String url;
  List<Tag> tags;

}
