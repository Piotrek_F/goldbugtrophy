package pl.codehussar.goldbugtrophy.model;

import java.io.Serializable;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Setter
@Getter
@NoArgsConstructor
@AllArgsConstructor
public class SendText implements Serializable {

  String userId;
  String url;
  String text;
  boolean isFake;
}
