package pl.codehussar.goldbugtrophy;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class GoldbugtrophyApplication {

  public static void main(String[] args) {
    SpringApplication.run(GoldbugtrophyApplication.class, args);
  }

}
