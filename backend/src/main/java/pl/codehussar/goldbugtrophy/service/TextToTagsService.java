package pl.codehussar.goldbugtrophy.service;

import com.textrazor.AnalysisException;
import com.textrazor.NetworkException;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collector;
import java.util.stream.Collectors;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import pl.codehussar.goldbugtrophy.model.InformationsPerUrl;
import pl.codehussar.goldbugtrophy.model.SendText;
import com.textrazor.TextRazor;
import com.textrazor.annotations.Entity;
import com.textrazor.annotations.AnalyzedText;
import pl.codehussar.goldbugtrophy.model.Tag;


@Slf4j
@Service
@RequiredArgsConstructor
public class TextToTagsService {

  public List<Tag> getTagsFromText(SendText text) {

    TextRazor client = new TextRazor("602864b52d7897f429efc3ed78a32fd7c96e09aa3bc707699402b820");

    client.addExtractor("words");
    client.addExtractor("entities");

    AnalyzedText response = null;
    try {
      response = client.analyze(
          text.getText());
    } catch (NetworkException e) {
      e.printStackTrace();
    } catch (AnalysisException e) {
      e.printStackTrace();
    }
    List<Tag> out = new ArrayList<>();
    for (Entity entity : response.getResponse().getEntities()) {
      out.add(new Tag(entity.getMatchingWords().size(), entity.getEntityId()));
      System.out.println("Matched Entity: " + entity.getEntityId() + " words: " + entity.getMatchingWords().size());
    }
    out = out.stream().sorted(Comparator.comparingInt(Tag::getCount).reversed()).collect(Collectors.toList());
    return out;
  }

}
