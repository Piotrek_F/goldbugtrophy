package pl.codehussar.goldbugtrophy.service;

import java.io.IOException;
import java.security.GeneralSecurityException;
import java.util.ArrayList;
import java.util.List;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.configurationprocessor.json.JSONArray;
import org.springframework.boot.configurationprocessor.json.JSONException;
import org.springframework.boot.configurationprocessor.json.JSONObject;
import org.springframework.stereotype.Service;
import pl.codehussar.goldbugtrophy.model.SendText;
import pl.codehussar.goldbugtrophy.model.InformationsPerUrl;
import pl.codehussar.goldbugtrophy.model.Tag;
import pl.codehussar.goldbugtrophy.model.User;

@Slf4j
@Service
@RequiredArgsConstructor
public class MainService {

  private List<User> listOfUsers = new ArrayList<>();
  private final TranslateService translateService;
  private final TextToTagsService textToTagsService;
  private final HashService hashService;

  public String getId() {
    String hash = hashService.getHash();
    User user = new User(hash, new ArrayList<>(), new ArrayList<>());
    listOfUsers.add(user);
    return hash;
  }

  public void sendText(SendText text) {
    try {
      text.setText(translateService.translateToEnglish(text));
    } catch (IOException e) {
      e.printStackTrace();
    } catch (GeneralSecurityException e) {
      e.printStackTrace();
    }
    List<Tag> tags = textToTagsService.getTagsFromText(text);
    User user = findUser(text.getUserId());
    if (user == null) {
      log.info("There was some fuckup");
      return;
    }
    log.info(user.getId());
    if (text.isFake()) {
      user.getInformationsPerUrlsFalse().add(new InformationsPerUrl(text.getUrl(), tags));
    } else {
      user.getInformationsPerUrls().add(new InformationsPerUrl(text.getUrl(), tags));
    }
  }

  private User findUser(String userId) {
    for (User user : listOfUsers) {
      if (user.getId().equals(userId)) {
        return user;
      }
    }
    return null;
  }

  public List<InformationsPerUrl> getTagsByUser(String userId, boolean isFalse) {
    User user = findUser(userId);
    if (user == null) {
      log.error("User wasn't found ");
      return null;
    }
    if (isFalse) {
      return user.getInformationsPerUrlsFalse();
    } else {
      return user.getInformationsPerUrls();
    }
  }

  public static Object toJSON(Object object) throws JSONException {

    JSONArray json = new JSONArray();
    for (Object value : ((Iterable) object)) {
      json.put(toJSON(value));
    }
    return json;
  }
}
