package pl.codehussar.goldbugtrophy.service;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Date;
import javax.xml.bind.DatatypeConverter;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

@Slf4j
@RequiredArgsConstructor
@Service
public class HashService {

  public String getHash() {
    Date date = new Date();

    MessageDigest md = null;
    try {
      md = MessageDigest.getInstance("MD5");
    } catch (NoSuchAlgorithmException e) {
      return date.toString() + "pupcia";
    }
    md.update(date.toString().getBytes());
    byte[] digest = md.digest();
    String myHash = DatatypeConverter
        .printHexBinary(digest).toUpperCase();
    log.info(date.toString());
    log.info(myHash);
    return myHash;
  }
}
