package pl.codehussar.goldbugtrophy.service;


import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringEscapeUtils;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;
import pl.codehussar.goldbugtrophy.model.SendText;
import java.io.IOException;
import java.security.GeneralSecurityException;
import java.util.Arrays;

import com.google.api.client.googleapis.javanet.GoogleNetHttpTransport;
import com.google.api.client.json.gson.GsonFactory;
import com.google.api.services.translate.Translate;
import com.google.api.services.translate.model.TranslationsListResponse;
import com.google.api.services.translate.model.TranslationsResource;

@Slf4j
@Service
@RequiredArgsConstructor
public class TranslateService {

  public String translateToEnglish(SendText text) throws IOException, GeneralSecurityException {

    Translate t = new Translate.Builder(
        GoogleNetHttpTransport.newTrustedTransport()
        , GsonFactory.getDefaultInstance(), null)
        // Set your application name
        .setApplicationName("My Project 30839")
        .build();

    Translate.Translations.List list = t.new Translations().list(
        Arrays.asList(
            // Pass in list of strings to be translated
            text.getText()),
        // Target language
        "ENG");

    // TODO: Set your API-Key from https://console.developers.google.com/
    list.setKey("AIzaSyCsbu7ViH_-pjcONXm4C3ZAwEmk1pz02o0");
    TranslationsListResponse response = list.execute();
    String translatedText = response.getTranslations().get(0).getTranslatedText();
    return StringEscapeUtils.unescapeHtml4(translatedText);
  }
}