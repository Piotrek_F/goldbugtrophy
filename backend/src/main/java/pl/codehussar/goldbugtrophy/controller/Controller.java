package pl.codehussar.goldbugtrophy.controller;

import java.util.List;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import pl.codehussar.goldbugtrophy.model.InformationsPerUrl;
import pl.codehussar.goldbugtrophy.model.SendText;
import pl.codehussar.goldbugtrophy.service.MainService;

@Slf4j
@RequiredArgsConstructor
@Service
@RequestMapping("/")
public class Controller {

  private final MainService mainService;

  @RequestMapping(value = "/testapp", method = RequestMethod.GET)
  public ResponseEntity<String> testRestartConfiguration() {
    log.info("TestApp was run! Application works");
    return new ResponseEntity<String>("Aplikacja dziala!", HttpStatus.OK);
  }

  @RequestMapping(value = "/getID", method = RequestMethod.GET)
  public ResponseEntity<String> getId(){
   return new ResponseEntity<>(mainService.getId(), HttpStatus.OK);
  }

  @RequestMapping(value = "/sendText", method = RequestMethod.POST)
  public ResponseEntity sendText(@RequestBody SendText text){
    mainService.sendText(text);
    return new ResponseEntity(HttpStatus.OK);
  }

  @RequestMapping(value = "/getTags", method = RequestMethod.GET)
  public @ResponseBody List<InformationsPerUrl> getTags(@RequestParam("userId") String id, @RequestParam("isFalse") boolean isFalse){
    List<InformationsPerUrl> tagsByUser = mainService.getTagsByUser(id, isFalse);
    return tagsByUser;
//    String json = new Gson().toJson(tagsByUser);
//    log.info(json);
//    return new ResponseEntity<>(json, HttpStatus.OK);
  }
}
