  // ##############################
// // // Tasks for TasksCard - see Dashboard view
// #############################

var bugs = [
  {
    name: "Politics", count: 10
  },
  {
    name: "Sport", count: 10
  },
  {
    name: "Weather", count: 10
  },
  {
    name: "Information technology", count: 10
  },
  {
    name: "News", count: 10
  }
];
var website = [
  "Flooded: One year later, assessing what was lost and what was found when a ravaging rain swept through metro Detroit",
  'Sign contract for "What are conference organizers afraid of?"'
];
var server = [
  "Lines From Great Russian Literature? Or E-mails From My Boss?",
  "Flooded: One year later, assessing what was lost and what was found when a ravaging rain swept through metro Detroit",
  'Sign contract for "What are conference organizers afraid of?"'
];

module.exports = {
  // these 3 are used to create the tasks lists in TasksCard - Dashboard view
  bugs,
  website,
  server
};
